﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityCommon
{
    public class ResourcesManager : MonoBehaviour
    {
        public static ResourcesManager Instance;

        public List<ResourcesCollection> Collections;

        public Transform GetGDTemp()
        {
            return m_gdTempParent;
        }

        private void Awake()
        {
            Instance = this;

            foreach (var item in Collections)
            {
                RegisterCollection(item.UniqueIdentifier, item);
            }

            GameObject gdGO = GameObject.Find("_GD");

            if (gdGO == null)
                m_gdParent = new GameObject("_GD").transform;
            else
                m_gdParent = gdGO.transform;

            gdGO = GameObject.Find("_GDTEMP");

            if (gdGO == null)
                m_gdTempParent = new GameObject("_GDTEMP").transform;
            else
                m_gdTempParent = gdGO.transform;
        }

        public void RegisterCollection( int identifier , ResourcesCollection collection )
        {
            if (m_defaultCollection == null)
                m_defaultCollection = collection;

            m_collections.Add(identifier, collection);
        }

        public UnityEngine.Object GetResource(Enum enumValue)
        {
            return m_defaultCollection.GetPrefab(enumValue);
        }

        public GameObject GetPrefab(Enum enumValue)
        {
            return m_defaultCollection.GetPrefab(enumValue) as GameObject;
        }

        public GameObject GetPrefab(Enum enumValue , int collectionIdentifier)
        {
            return m_collections[collectionIdentifier].GetPrefab(enumValue) as GameObject;
        }

        public T Instantiate<T>(Enum enumValue)
        {
            return (m_defaultCollection.Instantiate(enumValue)  as GameObject).GetComponent<T>();
        }

        public GameObject Instantiate(Enum enumValue)
        {
            return (m_defaultCollection.Instantiate(enumValue) as GameObject);
        }

        public UnityEngine.Object InstantiateObject(Enum enumValue)
        {
            return (m_defaultCollection.Instantiate(enumValue) );
        }

        public GameObject Instantiate(Enum enumValue , int collectionIdentifier)
        {
            return (m_collections[collectionIdentifier].Instantiate(enumValue) as GameObject);
        }

        public T Instantiate<T>(Enum enumValue, int collectionIdentifier)
        {
            return (m_collections[collectionIdentifier].Instantiate(enumValue) as GameObject).GetComponent<T>();
        }

        public T InstantiateGD<T>(Enum enumValue)
        {
            GameObject go = m_defaultCollection.Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdParent);
            return go.GetComponent<T>();
        }

        public GameObject InstantiateGD(Enum enumValue)
        {
            GameObject go = m_defaultCollection.Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdParent);
            return go;
        }

        public T InstantiateGD<T>(Enum enumValue, int collectionIdentifier)
        {
            GameObject go = m_collections[collectionIdentifier].Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdParent);
            return go.GetComponent<T>();
        }

        public GameObject InstantiateGD(Enum enumValue, int collectionIdentifier)
        {
            GameObject go = m_collections[collectionIdentifier].Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdParent);
            return go;
        }


        public T InstantiateGDTemp<T>(Enum enumValue)
        {
            GameObject go = m_defaultCollection.Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdTempParent);
            return go.GetComponent<T>();
        }

        public GameObject InstantiateGDTemp(Enum enumValue)
        {
            GameObject go = m_defaultCollection.Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdTempParent);
            return go;
        }

        public T InstantiateGDTemp<T>(Enum enumValue, int collectionIdentifier)
        {
            GameObject go = m_collections[collectionIdentifier].Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdTempParent);
            return go.GetComponent<T>();
        }

        public GameObject InstantiateGDTemp(Enum enumValue, int collectionIdentifier)
        {
            GameObject go = m_collections[collectionIdentifier].Instantiate(enumValue) as GameObject;
            go.transform.SetParent(m_gdTempParent);
            return go;
        }

        public void CleanGDTemp()
        {
            for (int i = m_gdTempParent.childCount - 1; i >= 0; i--)
            {
               GameObject.Destroy( m_gdTempParent.GetChild(i).gameObject);
            }
        }

        ResourcesCollection m_defaultCollection;
        Dictionary<int, ResourcesCollection> m_collections = new Dictionary<int, ResourcesCollection>();
        Transform m_gdParent;
        Transform m_gdTempParent;
    }

}