﻿using UnityEngine;
using System.Collections;

namespace UnityCommon.Inputs
{
    public class InputsMonoBehaviour : MonoBehaviour
    {

        void Update()
        {
            InputsManager.Instance.Update();
        }

    }
}