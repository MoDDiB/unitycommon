﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace UnityCommon.Graphics
{
    public class SpriteAnimationUI : MonoBehaviour
    {
        public SpritesAnimationDatas Datas
        {
            get
            {
                return m_datas;
            }
            set
            {
                m_datas = value;
                Initialize();
            }
        }


        void Start()
        {

            if ( Datas == null )
            {
              //  Debug.LogError("Can't find animation datas on " + name);
                return;
            }


            Initialize();
        }

        private void Initialize()
        {
            m_image = GetComponent<Image>();

            if (m_datas == null)
            {
              //  Debug.LogError("Can't find animation datas on m_datas " + name);
                return;
            }

            m_currentDatas = m_datas.Animations[0];

            m_animationTime = 1.0f / (float)m_currentDatas.FPS;

            m_timer = (float)(UnityEngine.Random.Range(0, m_currentDatas.Sprites.Count)) * m_animationTime;

            if (m_currentDatas.Sprites.Count > 0)
                m_image.sprite = m_currentDatas.Sprites[0];
        }

        void Update()
        {
            if (m_currentDatas == null)
                return;

            int currentIndex = (int)(m_timer / m_animationTime);

            if (currentIndex != m_previousIndex)
            {
                currentIndex = currentIndex % m_currentDatas.Sprites.Count;

                m_previousIndex = currentIndex;

                if (m_image != null)
                    m_image.sprite = m_currentDatas.Sprites[currentIndex];

            }

            m_timer += Time.deltaTime;
        }

        Image m_image;

        int m_previousIndex;
        float m_timer;
        float m_animationTime;

        SpritesAnimationDatas.AnimationDatas m_currentDatas;

        [SerializeField]
        private SpritesAnimationDatas m_datas;

    }

}