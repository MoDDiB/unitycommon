﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnityCommon.Graphics.PixelArt
{

    public class PixelArtSnaper : EditorWindow
    {
        [MenuItem("Pixel/PixelSnap")]
        private static void SnapPixels()
        {
            GameObject artParent = GameObject.Find("_Art");

            if (artParent == null)
            {
                Debug.LogError("can't find '_Art'");
                return;
            }

            foreach (var childTransform in artParent.GetComponentsInChildren<Transform>())
            {
                float x = childTransform.transform.localPosition.x - (childTransform.transform.localPosition.x % 0.01f);
                float y = childTransform.transform.localPosition.y - (childTransform.transform.localPosition.y % 0.01f);

                childTransform.transform.localPosition = new Vector3(x, y, 0);
            }





        }

    }

}