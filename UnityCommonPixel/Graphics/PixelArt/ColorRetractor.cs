﻿using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace UnityCommon.Graphics.PixelArt
{

    internal class ColorRetractor : EditorWindow
    {

        #region Public Methods

        [MenuItem("Window/Sprite Color Retractor")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(ColorRetractor));
        }

        #endregion Public Methods

        #region Private Methods

        private void Update()
        {
            Repaint();
        }

        int ColorFloatToInt(float value)
        {
            return (int)(value * 255.0f);
        }

        float ColorIntToFloat(int value)
        {
            return (float)(value / 255.0f);
        }


        private void OnGUI()
        {
            if (GUILayout.Button("Apply"))
            {

                Dictionary<int, List<Color>> colors = new Dictionary<int, List<Color>>();

                foreach (var go in Selection.objects)
                {
                    Texture2D texture = go as Texture2D;

                    string path = AssetDatabase.GetAssetPath(texture);
                    Debug.Log(texture);

                    var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
                    textureImporter.isReadable = true; //apparently this must be before the AssetDatabase.ImportAsset(...) call
                    AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);

                    for (int x = 0; x < texture.width; x++)
                    {
                        for (int y = 0; y < texture.height; y++)
                        {
                            Color c = texture.GetPixel(x, y);

                            if (c.a <= 0.001f)
                                continue;

                            int red = ColorFloatToInt(c.r);

                            if (colors.ContainsKey(red) == false)
                            {
                                colors.Add(red, new List<Color>());
                            }

                            if (colors[red].Contains(c) == false)
                            {
                                colors[red].Add(c);
                            }
                        }
                    }

                    textureImporter.isReadable = false; //apparently this must be before the AssetDatabase.ImportAsset(...) call
                    AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
                }

                System.Collections.Generic.List<ColorToModify> colorsToMove = new List<ColorToModify>();

                foreach (var item in colors.Values)
                {
                    for (int i = 1; i < item.Count; i++)
                    {
                        colorsToMove.Add(new ColorToModify(item[i]));
                    }

                    if (item.Count > 1)
                    {
                        for (int i = item.Count - 1; i >= 1; i--)
                        {
                            item.RemoveAt(i);
                        }
                    }
                }

                foreach (var colorToModify in colorsToMove)
                {
                    int red = ColorFloatToInt(colorToModify.Color.r);
                    int offset = 1;

                    if (red > 128)
                        offset = -1;

                    int newRed = red + offset;

                    while (colors.ContainsKey(newRed))
                    {
                        newRed += offset;
                    }

                    if (newRed > 255 || newRed < 0)
                        Debug.LogError("Can't find an available color");

                    colorToModify.NewColor = new Color(ColorIntToFloat(newRed), colorToModify.Color.g, colorToModify.Color.b, colorToModify.Color.a);

                    colors.Add(newRed, new List<Color>());
                }


                foreach (var go in Selection.objects)
                {
                    Texture2D texture = go as Texture2D;

                    string path = AssetDatabase.GetAssetPath(texture);

                    var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
                    textureImporter.isReadable = true; //apparently this must be before the AssetDatabase.ImportAsset(...) call
                    AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);


                    for (int x = 0; x < texture.width; x++)
                    {
                        for (int y = 0; y < texture.height; y++)
                        {
                            Color c = texture.GetPixel(x, y);
                            ColorToModify colorToModify;

                            if (FindInColorToModify(c, colorsToMove, out colorToModify) == false)
                            {
                                continue;
                            }

                            if (colorToModify.NewColor != null)
                            {
                                texture.SetPixel(x, y, colorToModify.NewColor.Value);
                            }
                        }
                    }

                    texture.Apply();

                    SaveTexture(texture, path);

                    textureImporter.isReadable = false; //apparently this must be before the AssetDatabase.ImportAsset(...) call
                    AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);

                    EditorUtility.SetDirty(texture);

                }

                AssetDatabase.SaveAssets();
            }

        }

        protected void SaveTexture(Texture2D texture, string path)
        {
            File.WriteAllBytes(path, texture.EncodeToPNG());
        }

        bool FindInColorToModify(Color colorToFind, List<ColorToModify> colors, out ColorToModify colorToModify)
        {
            foreach (var color in colors)
            {
                if (color.Color == colorToFind)
                {
                    colorToModify = color;
                    return true;
                }
            }

            colorToModify = new ColorToModify(Color.red);

            return false;
        }


        #endregion Private Methods

        #region Private Fields

        private static Color _borderColor = Color.gray;
        private static float _shade = 0.7f;
        private static Color _frameColor = new Color(_shade, _shade, _shade);

        private static Color _currentPivotColor = Color.green;
        private static Color _mousePosColor = Color.gray;
        private static Color _newPivotColor = Color.blue;
        private static Color _misplacedPivotColor = Color.red;

        #endregion Private Fields


        class ColorToModify
        {
            public Color Color;
            public Color? NewColor;

            public ColorToModify(Color color)
            {
                Color = color;
            }
        }

    }
}