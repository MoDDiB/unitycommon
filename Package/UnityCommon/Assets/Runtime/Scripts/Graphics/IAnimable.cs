﻿namespace UnityCommon
{
    public interface IAnimable
    {
        public void PlayAnimation(string name, bool loop);
    }
}