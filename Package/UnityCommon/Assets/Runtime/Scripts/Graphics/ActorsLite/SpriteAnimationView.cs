﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace UnityCommon.Graphics.Actors
{
    [Serializable]
    public class SpriteAnimationView
    {
        public List<Sprite> Sprites;
    }

}