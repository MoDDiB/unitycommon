﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UnityCommon.Graphics
{
    public class SpriteAnimation : MonoBehaviour
    {
        public SpritesAnimationDatas Datas;
        public bool Manual;


        void Initialize()
        {
            m_renderer = GetComponent<SpriteRenderer>();

            if (Datas == null || Datas.Animations.Count == 0)
            {
                Debug.LogError("Can't find animation datas on " + name);
            }

            if (m_renderer == null)
            {
                Debug.LogError("Can't find renderer on " + name);
            }

            m_currentDatas = Datas.Animations[0];

            m_animationTime = 1.0f / (float)m_currentDatas.FPS;

            m_timer = (float)(Random.Range(0, m_currentDatas.Sprites.Count)) * m_animationTime;

            if (m_currentDatas.Sprites.Count > 0)
                m_renderer.sprite = m_currentDatas.Sprites[0];

            m_initialized = true;
        }

        void Start()
        {
            if (m_initialized == false)
                Initialize();
        }

        void Update()
        {
            if (Manual)
                return;

            int currentIndex = (int)(m_timer / m_animationTime);

            if (currentIndex != m_previousIndex)
            {
                currentIndex = currentIndex % m_currentDatas.Sprites.Count;

                m_previousIndex = currentIndex;

                if (m_renderer != null)
                    m_renderer.sprite = m_currentDatas.Sprites[currentIndex];


            }

            m_timer += Time.deltaTime;
        }

        public void UpdateManual(int animationIndex , float timer)
        {
            m_currentDatas = Datas.Animations[animationIndex];
            m_animationTime = 1.0f / (float)m_currentDatas.FPS;

            m_timer = timer;

            int currentIndex = (int)(m_timer / m_animationTime);

            if (currentIndex != m_previousIndex || m_currentDatas != m_oldDatas)
            {
                currentIndex = currentIndex % m_currentDatas.Sprites.Count;

                m_previousIndex = currentIndex;

                if (m_renderer != null)
                    m_renderer.sprite = m_currentDatas.Sprites[currentIndex];


            }

            m_oldDatas = m_currentDatas;
        }

        public void SetAnimationIndex(int index)
        {
            if (m_initialized == false)
                Initialize();

            m_previousIndex = -1;

            m_currentDatas = Datas.Animations[index];

            m_animationTime = 1.0f / (float)m_currentDatas.FPS;

            m_timer = 0;

            if (m_currentDatas.Sprites.Count > 0)
                m_renderer.sprite = m_currentDatas.Sprites[0];
        }

        SpriteRenderer m_renderer;

        int m_previousIndex = -1;
        float m_timer;
        float m_animationTime;

        SpritesAnimationDatas.AnimationDatas m_currentDatas;
        SpritesAnimationDatas.AnimationDatas m_oldDatas;
        bool m_initialized;
    }

}