using UnityEngine;

namespace UnityCommon
{
    public class ManagerChecker : MonoBehaviour
    {
        public GameObject ManagerToCheck;
        public bool DontDestroy;
        public bool InitialChecker;
        public bool AutoStart = true;

        void Awake()
        {
            if (AutoStart)
                CheckManager();

        }


        public void CheckManager()
        {
            if (InitialChecker)
            {
                InstantiateManager();
            }
            else if (Application.isEditor)
            {
                MonoBehaviour behavior = ManagerToCheck.GetComponents<MonoBehaviour>()[0];

                System.Type behaviorType = behavior.GetType();
                Object obj = GameObject.FindObjectOfType(behaviorType);

                if (obj == null)
                {
                    Debug.Log("[Manager] Create not initial manager - This should be in the editor only : " + behaviorType);
                    InstantiateManager();
                }

            }
        }

        private void InstantiateManager()
        {
            GameObject newObj = GameObject.Instantiate(ManagerToCheck, transform) as GameObject;

            if (DontDestroy == true)
            {
                newObj.transform.parent = null;
                GameObject.DontDestroyOnLoad(newObj);
            }
        }
    }

}