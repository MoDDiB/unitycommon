using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCommon
{
    public class AutoGlobalRegister : MonoBehaviour
    {
        protected virtual void Awake()
        {
            GlobalObject.Register(this);
        }


        protected virtual void OnDestroy()
        {
            GlobalObject.UnRegister(this);
        }
    }
}