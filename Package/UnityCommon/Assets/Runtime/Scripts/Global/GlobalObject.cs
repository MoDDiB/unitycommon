using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Common.Tools;
using UnityEngine;


public class GlobalObject : MonoBehaviour
{
    public static GlobalObject Instance
    {
        get
        {
            if (s_instance == null)
            {
                //  Debug.LogError("There's no GlobalObject on the scene");
                GameObject go = new GameObject("GlobalObject");
                return go.AddComponent<GlobalObject>();
            }

            return s_instance;
        }
    }

    public static bool InstanceAlive
    {
        get => s_instance != null;
    }

    static GlobalObject s_instance;

    public List<GameObject> ObjectsToCheck = new List<GameObject>();
#if UNITY_EDITOR
    public List<GameObject> ObjectsToCheckEditor = new List<GameObject>();
#endif
    public List<ScriptableObject> Data = new List<ScriptableObject>();
    public bool ForceInstantiate;
    public bool DontDestroy;

    Dictionary<Type, MonoBehaviour> _objects = new Dictionary<Type, MonoBehaviour>();

    void Awake()
    {
        if (s_instance != null)
        {
            Debug.LogError("There's more than one on the scene");
        }

        s_instance = this;

        foreach (var item in ObjectsToCheck)
        {
            MonoBehaviour component = item.GetComponent<MonoBehaviour>();
            _objects.Add(component.GetType(), component);
        }

#if UNITY_EDITOR
        foreach (var item in ObjectsToCheckEditor)
        {
            MonoBehaviour component = item.GetComponent<MonoBehaviour>();
            _objects.Add(component.GetType(), component);
        }
#endif

        CheckManager();
    }

    public static void Register(MonoBehaviour component)
    {
        Instance._objects.Add(component.GetType(), component);
    }

    public static void UnRegister(MonoBehaviour component)
    {
        if (s_instance == null) return;

        s_instance._objects.Remove(component.GetType());
    }

    public static T Get<T>() where T : MonoBehaviour
    {
        Type type = typeof(T);

        if (Instance._objects.TryGetValue(type, out MonoBehaviour component) == false)
        {
            Debug.LogError($"Can't find type {type}");
        }

        return component as T;
    }

    public static T TryGet<T>() where T : MonoBehaviour
    {
        Type type = typeof(T);

        if (Instance._objects.TryGetValue(type, out MonoBehaviour component) == false)
        {
        }

        return component as T;
    }

    public static T GetData<T>() where T : ScriptableObject
    {
        Type type = typeof(T);

        ScriptableObject obj = Instance.Data.Find((ScriptableObject s) => s.GetType() == type);

        return obj as T;
    }

    public void CheckManager()
    {
        if (ForceInstantiate)
        {
            foreach (var item in ObjectsToCheck)
            {
                InstantiateManager(item);
            }
        }
        else if (Application.isEditor)
        {
            foreach (var item in ObjectsToCheck)
            {
                MonoBehaviour behavior = item.GetComponents<MonoBehaviour>()[0];

                System.Type behaviorType = behavior.GetType();
                UnityEngine.Object obj = GameObject.FindObjectOfType(behaviorType);

                if (obj == null)
                {
                    Debug.Log("[Manager] Create not initial manager - This should be in the editor only : " +
                              behaviorType);
                    InstantiateManager(item);
                }
            }

#if UNITY_EDITOR
            foreach (var item in ObjectsToCheckEditor)
            {
                MonoBehaviour behavior = item.GetComponents<MonoBehaviour>()[0];

                System.Type behaviorType = behavior.GetType();
                UnityEngine.Object obj = GameObject.FindObjectOfType(behaviorType);

                if (obj == null)
                {
                    Debug.Log("[Manager] Create not initial manager - This should be in the editor only : " +
                              behaviorType);
                    InstantiateManager(item);
                }
            }
#endif
        }
    }

    private void InstantiateManager(GameObject go)
    {
        GameObject newObj = GameObject.Instantiate(go) as GameObject;

        if (DontDestroy == true)
        {
            // newObj.transform.parent = null;
            GameObject.DontDestroyOnLoad(newObj);
        }
    }
}