﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnumFactoryDatas : ScriptableObject
{
    public List<String> EnumValues;
    public string FactoryName;
    public string ClassPath;


}
