﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCommon.Tools
{

    public class PathFinder
    {
        public static readonly Vector2Int[] Directions = new Vector2Int[]
        { new Vector2Int(1, 0), new Vector2Int(-1, 0), new Vector2Int(0, 1), new Vector2Int(0, -1) };

        public static readonly Vector2Int[] Directions8 = new Vector2Int[]
{ new Vector2Int(1, 0), new Vector2Int(-1, 0), new Vector2Int(0, 1), new Vector2Int(0, -1),
    new Vector2Int(-1, 1), new Vector2Int(1, 1), new Vector2Int(1, -1), new Vector2Int(-1, -1),
};

        public static readonly Vector2Int[] Corners = new Vector2Int[]
{
    new Vector2Int(-1, 1), new Vector2Int(1, 1), new Vector2Int(1, -1), new Vector2Int(-1, -1),
};

        public void Initialize(int width, int height)
        {
            m_width = width;
            m_height = height;

            m_cost = new int[width, height];

            for (int x = 0; x < m_width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    m_cost[x, y] = 1;
                }
            }

        }

        public void SetWalkable(int x, int y, int cost)
        {
            m_cost[x, y] = cost;
        }


        public List<Vector2Int> GetAvailableCases(Vector2Int start, int distance)
        {
            List<Vector2Int> path = new List<Vector2Int>();

            m_openNodes.Clear();
            m_closedNodes.Clear();

            Node nodeStart = new Node(start);
            nodeStart.CostFromStart = 0;
            m_openNodes.Add(nodeStart);


            while (m_openNodes.Count > 0 && m_openNodes.Count < 500)
            {
                Node currentNode = m_openNodes[0];

                foreach (var direction in Directions)
                {
                    int x = currentNode.Position.x + direction.x;
                    int y = currentNode.Position.y + direction.y;

                    Vector2Int neighbourPosition = new Vector2Int(x, y);

                    if (x >= 0 && x < m_width && y >= 0 && y < m_height)
                    {

                        float cost = m_cost[x, y];

                        int index;
                        Node neighbour = GetInClosed(x, y, out index);
                        float costFromStart = currentNode.CostFromStart + cost;



                        if (neighbour != null && costFromStart < neighbour.CostFromStart)
                        {
                            //  Debug.Log("test again " + neighbour.Position);
                            m_closedNodes.RemoveAt(index);
                        }
                        else if (neighbour != null)
                        {
                            continue;
                        }
                        else
                        {
                            neighbour = new Node(neighbourPosition);
                        }

                        neighbour.CostFromStart = currentNode.CostFromStart + cost;



                        if (costFromStart <= distance)
                        {
                            m_openNodes.Add(neighbour);
                            path.Add(neighbour.Position);
                        }
                    }
                }

                m_openNodes.Remove(currentNode);
                m_closedNodes.Add(currentNode);
            }



            return path;


        }


        public List<Vector2Int> GetPath(Vector2Int start, Vector2Int end)
        {
            List<Vector2Int> path = new List<Vector2Int>();

            m_openNodes.Clear();
            m_closedNodes.Clear();

            Node nodeStart = new Node(start);
            nodeStart.CostFromStart = 0;
            nodeStart.CostToEnd = GetDistanceToEnd(start, end);
            nodeStart.ToTalCost = nodeStart.CostFromStart + nodeStart.CostToEnd;
            m_openNodes.Add(nodeStart);

            Node lastNode = null;

            while (m_openNodes.Count > 0 && m_openNodes.Count < 500)
            {
                Node currentNode = GetBestNode();

                if (currentNode.Position.x == end.x && currentNode.Position.y == end.y)
                {
                    lastNode = currentNode;
                    break;
                }

                foreach (var direction in Directions)
                {
                    int x = currentNode.Position.x + direction.x;
                    int y = currentNode.Position.y + direction.y;

                    Vector2Int neighbourPosition = new Vector2Int(x, y);

                    if (x >= 0 && x < m_width && y >= 0 && y < m_height)
                    {

                        float cost = m_cost[x, y];

                        int index;
                        Node neighbour = GetInClosed(x, y, out index);

                        float costToEnd = GetDistanceToEnd(neighbourPosition, end);
                        float totalCost = currentNode.CostFromStart + cost + costToEnd;

                        //if (neighbour != null )
                        //    Debug.Log("totalcost " + totalCost + " neighbour.ToTalCost  " + neighbour.ToTalCost);


                        if (neighbour != null && totalCost < neighbour.ToTalCost)
                        {
                            //  Debug.Log("test again " + neighbour.Position);
                            m_closedNodes.RemoveAt(index);
                        }
                        else if (neighbour != null)
                        {
                            continue;
                        }
                        else
                        {
                            neighbour = new Node(neighbourPosition);
                        }

                        neighbour.PreviousNode = currentNode;
                        neighbour.CostFromStart = currentNode.CostFromStart + cost;
                        neighbour.CostToEnd = costToEnd;
                        neighbour.ToTalCost = neighbour.CostFromStart + neighbour.CostToEnd;


                        m_openNodes.Add(neighbour);
                    }
                }

                m_openNodes.Remove(currentNode);
                m_closedNodes.Add(currentNode);
            }

            while (lastNode != null)
            {
                path.Insert(0, lastNode.Position);

                lastNode = lastNode.PreviousNode;
            }

            if (path.Count > 0)
                path.RemoveAt(0);


            return path;
        }


        Node GetBestNode()
        {
            float bestDistannce = float.MaxValue;
            Node bestNode = null;

            foreach (var item in m_openNodes)
            {
                if (item.ToTalCost < bestDistannce)
                {
                    bestNode = item;
                    bestDistannce = item.ToTalCost;
                }
            }

            return bestNode;
        }

        Node GetInClosed(int x, int y, out int index)
        {
            for (int i = 0; i < m_closedNodes.Count; i++)
            {
                if (m_closedNodes[i].Position.x == x && m_closedNodes[i].Position.y == y)
                {
                    index = i;
                    return m_closedNodes[i];
                }
            }

            index = -1;
            return null;
        }

        float GetDistanceToEnd(Vector2Int start, Vector2Int end)
        {
            return (Mathf.Abs(end.x - start.x) + Mathf.Abs(end.y - start.y)) * 100000;
        }



        class Node
        {
            public Vector2Int Position;
            public float CostFromStart;
            public float CostToEnd;
            public float ToTalCost;

            public Node PreviousNode;


            public Node(Vector2Int position)
            {
                Position = position;
            }

        }

        List<Node> m_openNodes = new List<Node>();
        List<Node> m_closedNodes = new List<Node>();

        int[,] m_cost;

        int m_width;
        int m_height;
    }

}




///**
//* Calculates the Field Of View for the provided map from the given x, y
//* coordinates. Returns a lightmap for a result where the values represent a
//* percentage of fully lit.
//*
//* A value equal to or below 0 means that cell is not in the
//* field of view, whereas a value equal to or above 1 means that cell is
//* in the field of view.
//*
//* @param resistanceMap the grid of cells to calculate on where 0 is transparent and 1 is opaque
//* @param startx the horizontal component of the starting location
//* @param starty the vertical component of the starting location
//* @param radius the maximum distance to draw the FOV
//* @param radiusStrategy provides a means to calculate the radius as desired
//* @return the computed light grid
//*/
//public float[][] calculateFOV(float[][] resistanceMap, int startx, int starty, float radius, RadiusStrategy rStrat)
//{
//    this.startx = startx;
//    this.starty = starty;
//    this.radius = radius;
//    this.rStrat = rStrat;
//    this.resistanceMap = resistanceMap;

//    width = resistanceMap.length;
//    height = resistanceMap[0].length;
//    lightMap = new float[width][height];

//    lightMap[startx][starty] = force;//light the starting cell
//    for (Direction d : Direction.DIAGONALS) {
//    castLight(1, 1.0f, 0.0f, 0, d.deltaX, d.deltaY, 0);
//    castLight(1, 1.0f, 0.0f, d.deltaX, 0, 0, d.deltaY);
//}

//return lightMap;
//}

//private void castLight(int row, float start, float end, int xx, int xy, int yx, int yy)
//{
//    float newStart = 0.0f;
//    if (start < end)
//    {
//        return;
//    }
//    boolean blocked = false;
//    for (int distance = row; distance <= radius && !blocked; distance++)
//    {
//        int deltaY = -distance;
//        for (int deltaX = -distance; deltaX <= 0; deltaX++)
//        {
//            int currentX = startx + deltaX * xx + deltaY * xy;
//            int currentY = starty + deltaX * yx + deltaY * yy;
//            float leftSlope = (deltaX - 0.5f) / (deltaY + 0.5f);
//            float rightSlope = (deltaX + 0.5f) / (deltaY - 0.5f);

//            if (!(currentX >= 0 && currentY >= 0 && currentX < this.width && currentY < this.height) || start < rightSlope)
//            {
//                continue;
//            }
//            else if (end > leftSlope)
//            {
//                break;
//            }

//            //check if it's within the lightable area and light if needed
//            if (rStrat.radius(deltaX, deltaY) <= radius)
//            {
//                float bright = (float)(1 - (rStrat.radius(deltaX, deltaY) / radius));
//                lightMap[currentX][currentY] = bright;
//            }

//            if (blocked)
//            { //previous cell was a blocking one
//                if (resistanceMap[currentX][currentY] >= 1)
//                {//hit a wall
//                    newStart = rightSlope;
//                    continue;
//                }
//                else
//                {
//                    blocked = false;
//                    start = newStart;
//                }
//            }
//            else
//            {
//                if (resistanceMap[currentX][currentY] >= 1 && distance < radius)
//                {//hit a wall within sight line
//                    blocked = true;
//                    castLight(distance + 1, start, leftSlope, xx, xy, yx, yy);
//                    newStart = rightSlope;
//                }
//            }
//        }
//    }
//}