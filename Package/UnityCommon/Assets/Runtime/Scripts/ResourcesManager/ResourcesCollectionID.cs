﻿using System;
using UnityEngine.Rendering;

namespace UnityCommon
{
    [Serializable]
    public struct ResourcesCollectionID
    {
        public int Value;

        public ResourcesCollectionID(int value)
        {
            Value = value;
        }

        public static explicit operator int(ResourcesCollectionID id) => id.Value;
        public static explicit operator ResourcesCollectionID(int value) =>  new ResourcesCollectionID(value);
    }
}