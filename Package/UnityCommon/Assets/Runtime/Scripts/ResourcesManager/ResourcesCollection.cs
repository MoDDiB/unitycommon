﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityCommon
{

    public class ResourcesCollection : MonoBehaviour
    {
        public string Name;
      //  public int UniqueIdentifier;
        public ResourcesCollectionID UniqueIdentifierID;

        public List<Object> Objects;
        public List<string> AddToEnum;

        void Awake()
        {
            foreach (var obj in Objects)
            {
                m_objects.Add(obj.name.GetHashCode(), obj);
            }

        }

        public Object GetPrefab(System.Enum enumValue)
        {
            int index = System.Convert.ToInt32(enumValue);
            Object prefab = null;

            if (m_objects.TryGetValue(index, out prefab) == false)
            {
                Debug.LogError("Can't find " + enumValue + " in collection " + Name);
                return null;
            }

            return prefab;
        }

        public Object Instantiate(System.Enum enumValue)
        {
            int index = System.Convert.ToInt32(enumValue);
            Object prefab = null;

            if (m_objects.TryGetValue(index, out prefab) == false)
            {

                Debug.LogError("Can't find " + enumValue + " in collection " + Name);
                return null;
            }

            Object newObj = GameObject.Instantiate(prefab);

            return newObj;
        }

        Dictionary<int, Object> m_objects = new Dictionary<int, Object>();
    }
    
}