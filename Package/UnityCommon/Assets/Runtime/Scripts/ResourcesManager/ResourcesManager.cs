﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityCommon
{
    public class ResourcesManager : MonoBehaviour
    {
        public static ResourcesManager Instance;

        public List<ResourcesCollection> Collections;

        ResourcesCollection _defaultCollection;
        Dictionary<int, ResourcesCollection> _collections = new Dictionary<int, ResourcesCollection>();
        Transform _gdParent;
       // Transform _gdTempParent;

        public Transform GetGDParent()
        {
            return _gdParent;
        }

        //// TODO : remove ?
        //public Transform GetGDTemp()
        //{
        //    return _gdTempParent;
        //}

        private void Awake()
        {
            Instance = this;

            foreach (var item in Collections)
            {
                RegisterCollection((int) item.UniqueIdentifierID, item);
            }

            GameObject gdGO = GameObject.Find("_GD");

            if (gdGO == null)
                _gdParent = new GameObject("_GD").transform;
            else
                _gdParent = gdGO.transform;

            //gdGO = GameObject.Find("_GDTEMP");

            //if (gdGO == null)
            //    _gdTempParent = new GameObject("_GDTEMP").transform;
            //else
            //    _gdTempParent = gdGO.transform;
        }

        public void RegisterCollection( int identifier , ResourcesCollection collection )
        {
            if (_defaultCollection == null)
                _defaultCollection = collection;

            _collections.Add(identifier, collection);
        }

        public UnityEngine.Object GetResource(Enum enumValue)
        {
            return _defaultCollection.GetPrefab(enumValue);
        }

        public GameObject GetPrefab(Enum enumValue)
        {
            return _defaultCollection.GetPrefab(enumValue) as GameObject;
        }

        public GameObject GetPrefab(Enum enumValue , int collectionIdentifier)
        {
            return _collections[collectionIdentifier].GetPrefab(enumValue) as GameObject;
        }

        public T Instantiate<T>(Enum enumValue) where T : Component
        {
            return (_defaultCollection.Instantiate(enumValue)  as GameObject).GetComponent<T>();
        }

        public GameObject Instantiate(Enum enumValue)
        {
            return (_defaultCollection.Instantiate(enumValue) as GameObject);
        }

        public UnityEngine.Object InstantiateObject(Enum enumValue)
        {
            return (_defaultCollection.Instantiate(enumValue) );
        }

        public GameObject Instantiate(Enum enumValue , ResourcesCollectionID collectionIdentifier)
        {
            return (_collections[(int) collectionIdentifier].Instantiate(enumValue) as GameObject);
        }

        public T Instantiate<T>(Enum enumValue, ResourcesCollectionID collectionIdentifier) where T : Component
        {
            return (_collections[(int) collectionIdentifier].Instantiate(enumValue) as GameObject).GetComponent<T>();
        }

        public T InstantiateGD<T>(Enum enumValue) where T : Component
        {
            GameObject go = _defaultCollection.Instantiate(enumValue) as GameObject;
            go.transform.SetParent(_gdParent);
            return go.GetComponent<T>();
        }

        public GameObject InstantiateGD(Enum enumValue)
        {
            GameObject go = _defaultCollection.Instantiate(enumValue) as GameObject;
            go.transform.SetParent(_gdParent);
            return go;
        }

        public T InstantiateGD<T>(Enum enumValue, ResourcesCollectionID collectionIdentifier) where T : Component
        {
            GameObject go = _collections[(int) collectionIdentifier].Instantiate(enumValue) as GameObject;
            go.transform.SetParent(_gdParent);
            return go.GetComponent<T>();
        }

        public GameObject InstantiateGD(Enum enumValue, ResourcesCollectionID collectionIdentifier)
        {
            GameObject go = _collections[(int) collectionIdentifier].Instantiate(enumValue) as GameObject;
            go.transform.SetParent(_gdParent);
            return go;
        }


        //public T InstantiateGDTemp<T>(Enum enumValue) where T : Component
        //{
        //    GameObject go = _defaultCollection.Instantiate(enumValue) as GameObject;
        //    go.transform.SetParent(_gdTempParent);
        //    return go.GetComponent<T>();
        //}

        //public GameObject InstantiateGDTemp(Enum enumValue)
        //{
        //    GameObject go = _defaultCollection.Instantiate(enumValue) as GameObject;
        //    go.transform.SetParent(_gdTempParent);
        //    return go;
        //}

        //public T InstantiateGDTemp<T>(Enum enumValue, ResourcesCollectionID collectionIdentifier) where T : Component
        //{
        //    GameObject go = _collections[(int) collectionIdentifier].Instantiate(enumValue) as GameObject;
        //    go.transform.SetParent(_gdTempParent);
        //    return go.GetComponent<T>();
        //}

        //public GameObject InstantiateGDTemp(Enum enumValue, ResourcesCollectionID collectionIdentifier)
        //{
        //    GameObject go = _collections[(int) collectionIdentifier].Instantiate(enumValue) as GameObject;
        //    go.transform.SetParent(_gdTempParent);
        //    return go;
        //}

        //public void CleanGDTemp()
        //{
        //    for (int i = _gdTempParent.childCount - 1; i >= 0; i--)
        //    {
        //       GameObject.Destroy( _gdTempParent.GetChild(i).gameObject);
        //    }
        //}

    
    }

}