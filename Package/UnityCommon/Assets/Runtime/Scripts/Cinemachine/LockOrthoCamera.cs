using UnityEngine;
using Cinemachine;

/// <summary>
/// An add-on module for Cinemachine Virtual Camera that locks the camera's Y co-ordinate
/// </summary>
[SaveDuringPlay]
[AddComponentMenu("")] // Hide in menu
public class LockOrthoCamera : CinemachineExtension
{
    public Transform Min;
    public Transform Max;

    protected override void PostPipelineStageCallback(
        CinemachineVirtualCameraBase vcam,
        CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (stage == CinemachineCore.Stage.Finalize)
        {
            var pos = state.RawPosition;

            pos.x = Mathf.Max(pos.x, Min.position.x);
            pos.y = Mathf.Max(pos.y, Min.position.y);
            pos.x = Mathf.Min(pos.x, Max.position.x);
            pos.y = Mathf.Min(pos.y, Max.position.y);
            state.RawPosition = pos;
        }
    }
}