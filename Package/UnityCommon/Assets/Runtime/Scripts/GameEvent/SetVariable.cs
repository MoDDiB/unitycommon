﻿using System.Collections;
using System.Collections.Generic;
using UnityCommon;
using UnityCommon.GameEvent;
using UnityEngine;

[GameEffect("Common/SetVariable")]
public class SetVariable : GameEffect
{
    public string Name;
    public float Value;

    public override bool Execute(GameEventInstance gameEvent)
    {
        ScenesManagement.Instance.SetValue(Name, Value);

        return true;
    }

    public override string ToString()
    {
        return $"Set {Name}  {Value}";
    }

}
