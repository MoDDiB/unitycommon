//using System;
//using System.Collections;
//using System.Collections.Generic;
////using FMODUnity;
//using UnityCommon;
//using UnityEngine;

//[Serializable]
//public class PlaySound : GameEffect
//{
//    public EventReference SoundEvent;
//    public string Parameter;
//    public float Value;
//    public float MaxValue;
//    public ParameterType Type;
    
//    public enum ParameterType
//    {
//        Normal,
//        Increment
//    }
    
//    public override bool Execute(GameEventInstance gameEvent)
//    { 
//        float value = Value;
//        if (String.IsNullOrEmpty(Parameter))
//        {
//            FMODUnity.RuntimeManager.PlayOneShot(SoundEvent);
//        }
//        else
//        {
//            if (Type == ParameterType.Increment)
//            {
//                if (ScenesManagement.Instance.HasValue(Parameter))
//                {
//                    value = ScenesManagement.Instance.GetFloatValue(Parameter);
//                }
//                float newValue = value + 1.0f;
//                if (newValue > MaxValue)
//                    newValue = Value;
                    
//                ScenesManagement.Instance.SetValue(Parameter, newValue);
                
//            }
            
//            FMODHelper.PlayOneShotParameter(SoundEvent,Parameter,value);
//        }
        
        
//        //Debug.Log($"Play sound {SoundEvent}" + Time.frameCount + " " + value);
        
//        return true;
//    }

//    public override string ToString()
//    {
//#if UNITY_EDITOR
//        return $"Play sound {SoundEvent.Path}";
//#else
//        return $"Play sound {SoundEvent}";
//#endif
//    }
//}
