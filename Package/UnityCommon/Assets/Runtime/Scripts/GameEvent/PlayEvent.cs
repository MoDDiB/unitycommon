using System;
using System.Collections;
using System.Collections.Generic;
using UnityCommon;
using UnityEngine;


namespace UnityCommon
{
    namespace GameEvent
    {
        [Serializable]
        public class PlayEvent : GameEffect
        {
            public string Name;

            public override bool Execute(GameEventInstance gameEvent)
            {
                GlobalEvent.Play(Name);

                return true;
            }

            public override string ToString()
            {
                return $"Play event {Name}";
            }
        }

    }
}