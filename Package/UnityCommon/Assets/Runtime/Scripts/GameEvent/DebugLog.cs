﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace UnityCommon
{
    namespace GameEvent
    {
        class DebugLog : GameEffect
        {
            public string Text;

            public override bool Execute(GameEventInstance gameEvent)
            {
                Debug.Log(Text);
                return true;
            }


        }
    }
}
