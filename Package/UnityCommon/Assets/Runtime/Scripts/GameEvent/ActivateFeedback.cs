using System;

namespace UnityCommon
{
    namespace GameEvent
    {
        [Serializable]
        [GameEffect("GameObject/Activate", 255, 0, 0)]
        public class ActivateFeedback : GameEffect
        {
            public bool Activate;

            public override bool Execute(GameEventInstance gameEvent)
            {
                if (Activate == false)
                    gameEvent.AddParameter(gameEvent.GameObject);

                gameEvent.GameObject.SetActive(Activate);

                return true;
            }

            public override string ToString()
            {
                return Activate ? "Activate" : "Deactivate";
            }
        }
    }
}
