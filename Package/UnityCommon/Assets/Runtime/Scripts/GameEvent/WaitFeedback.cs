using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityCommon
{
    namespace GameEvent
    {
        [Serializable]
        [GameEffect("WaitTimer", 0, 255, 0)]
        public class WaitGameEffect : GameEffect
        {
            public float Timer;

            public override bool Execute(GameEventInstance gameEvent)
            {
                gameEvent.AddValue("Time", Time.deltaTime);

                if (gameEvent.GetValue("Time") >= Timer)
                {
                    gameEvent.SetValue("Time", 0);
                    return true;
                }

                return false;
            }

            public override string ToString()
            {
                return $"Wait {Timer}s";
            }
        }

    }
}
