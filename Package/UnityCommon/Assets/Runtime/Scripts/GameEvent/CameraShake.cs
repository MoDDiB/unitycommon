﻿using Cinemachine;

using UnityEngine;
namespace UnityCommon
{
    namespace GameEvent
    {

        [GameEffect("Common/CameraShake")]
        class CameraShake : GameEffect
        {
            public override bool Execute(GameEventInstance gameEvent)
            {
                CinemachineImpulseSource impulse = GameObject.FindObjectOfType<CinemachineImpulseSource>();
                impulse.GenerateImpulse();
                return true;
            }


        }
    }
}
