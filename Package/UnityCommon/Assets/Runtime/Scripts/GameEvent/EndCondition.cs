using System;
using System.Collections;
using System.Collections.Generic;
using Common.Tools;
using UnityEngine;


namespace UnityCommon
{
    namespace GameEvent
    {
        [Serializable]
        [GameEffect("Condition/EndIf", 255, 255, 0)]
        public class EndCondition : GameEffect
        {
            public override bool Execute(GameEventInstance gameEvent)
            {

                return true;
            }

            public override string ToString()
            {
                return "End IF ";
            }
        }
    }
}