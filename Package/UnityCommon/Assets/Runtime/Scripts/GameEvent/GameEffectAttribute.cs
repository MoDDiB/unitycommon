using System;
using UnityEngine;


namespace UnityCommon
{
    namespace GameEvent
    {

        [AttributeUsage(AttributeTargets.Class, Inherited = false)]
        public class GameEffectAttribute : Attribute
        {
            public Color Color => color;
            public String MenuName => menuName;

            private Color color = Color.white;
            private string menuName;

            public GameEffectAttribute()
            {
                color = Color.white;
            }

            public GameEffectAttribute(string menu)
            {
                menuName = menu;
            }

            public GameEffectAttribute(string menu, int r, int g, int b)
            {
                menuName = menu;
                color = new Color(r / 255f, g / 255f, b / 255f);
            }

            public GameEffectAttribute(int r, int g, int b)
            {
                color = new Color(r / 255f, g / 255f, b / 255f);
            }

        }

    }
}