using System;
using System.Collections;
using System.Collections.Generic;
using Common.Tools;
using UnityEngine;

namespace UnityCommon
{
    namespace GameEvent
    {
        [Serializable]
        public abstract class BaseConditionFeedback : GameEffect
        {
            public override bool Execute(GameEventInstance gameEvent)
            {

                return true;
            }

            public override string ToString()
            {
                return "Condition ";
            }
        }
    }
}