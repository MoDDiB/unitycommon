﻿
using UnityCommon.GameEvent;
using UnityEngine;

[GameEffect("Common/Condition/VariableCondition")]
public class VariableCondition : BaseConditionFeedback
{
    public enum ConditionType
    {
        Equal,
        Superior,
        Inferior
    }

    public string Name;
    public float Value;
    public ConditionType Type;

    public override bool Execute(GameEventInstance gameEvent)
    {
        if (Type == ConditionType.Equal)
            return Mathf.Abs(ScenesManagement.Instance.GetFloatValue(Name) - Value) < 0.001f;
        if (Type == ConditionType.Superior)
            return ScenesManagement.Instance.GetFloatValue(Name) > Value;
        if (Type == ConditionType.Inferior)
            return ScenesManagement.Instance.GetFloatValue(Name) < Value;

        throw new System.Exception("NO WAY");

    }

    public override string ToString()
    {
        if (Type == ConditionType.Equal)
            return $"IF {Name} == {Value}";
        if (Type == ConditionType.Superior)
            return $"IF {Name} > {Value}";
        if (Type == ConditionType.Inferior)
            return $"IF {Name} < {Value}";

        throw new System.Exception("NO WAY");

    }
}
