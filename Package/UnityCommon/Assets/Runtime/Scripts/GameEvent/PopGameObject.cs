﻿namespace UnityCommon.GameEvent
{
    [GameEffect("GameObject/PopGameObject", 255, 0, 0)]
    public class PopGameObject : GameEffect
    {
        
        public override bool Execute(GameEventInstance gameEvent)
        {
            gameEvent.PopGameObject();
            return true;
        }

        public override string ToString()
        {
            return $"Pop GameObject";
        }
    }
}