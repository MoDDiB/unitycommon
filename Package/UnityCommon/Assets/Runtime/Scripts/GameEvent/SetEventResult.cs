using System;
using System.Collections;
using System.Collections.Generic;
using UnityCommon;
using UnityEngine;

namespace UnityCommon
{
    namespace GameEvent
    {
        /// <summary>
        /// Returns a result to the launcher of the script
        /// </summary>
        [Serializable]
        [GameEffect("Common/SetEventResult")]
        public class SetEventResult : GameEffect
        {
            public bool Result;

            public override bool Execute(GameEventInstance gameEvent)
            {
                gameEvent.EventResult = Result;

                return true;
            }

            public override string ToString()
            {
                return $"Set result {Result}";
            }
        }

    }
}
