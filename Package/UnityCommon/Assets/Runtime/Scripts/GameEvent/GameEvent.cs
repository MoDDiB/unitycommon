using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityCommon
{
    namespace GameEvent
    {
        [CreateAssetMenu(menuName = "GameEvent")]
        [Serializable]
        public class GameEvent : ScriptableObject
        {
            [TextArea]
            public string Comments;

            [SerializeReference]
            public List<GameEffect> Feedbacks = new List<GameEffect>();


        }
    }
}