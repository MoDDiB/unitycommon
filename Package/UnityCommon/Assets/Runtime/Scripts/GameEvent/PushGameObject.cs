﻿namespace UnityCommon.GameEvent
{
    [GameEffect("GameObject/PushGameObject", 255, 255, 0)]
    public class PushGameObject : GameEffect
    {
        public string Name;
        
        public override bool Execute(GameEventInstance gameEvent)
        {
            gameEvent.PushGameObject(Name);
            return true;
        }

        public override string ToString()
        {
            return $"Push {Name}";
        }
    }
}