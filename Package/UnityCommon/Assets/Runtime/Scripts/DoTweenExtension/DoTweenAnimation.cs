using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoTweenAnimation : MonoBehaviour
{
    public enum TweenType
    {
        Fade,
    }

    public TweenType Type;
    public float To;
    public float Duration = 0.5f;
    public int Loop = 1;
    
    
    // Start is called before the first frame update
    void Start()
    {
        if (Type == TweenType.Fade)
        {
            GetComponent<SpriteRenderer>().DOFade(To, Duration).SetLoops(Loop, LoopType.Yoyo);
        }
    }


}
