//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Text;
//using GameCommon;

//using UnityEditor;
//using UnityEngine;

//public class SaveGame
//{
//    #if UNITY_EDITOR
//        public const string DirectoryName = "SaveDEV";
//    #else
//        public const string DirectoryName = "SaveGame";
//    #endif
    
    
//    static readonly string[] _slots = new string[] {"slot1", "slot2", "slot3"};

//    static string _currentSlot = "slot1";
//    const string _slotFileName = "Slot.dat";


//    public static bool SaveLoaded => _saveLoaded;
//    static bool _saveLoaded;

//    // public static bool HasProfile()
//    // {
//    //     string profilePath = GetProfileDataPath();
//    //     if (File.Exists(GetProfileDataPath()) == false)
//    //         return false;
//    //
//    //     string content = File.ReadAllText(profilePath);
//    //
//    //     ProfileData data = JsonUtility.FromJson<ProfileData>(content);
//    //
//    //     return data.SlotNames.Length > 0;
//    // }

//#if UNITY_EDITOR
//    [MenuItem("Tools/Save/Delete")]
//#endif
//    public static void DeleteSave()
//    {
//        if (Directory.Exists(GetSavePath()))
//        {
//            Directory.Delete(GetSavePath(), true);
            
//            if ( PlayerProfile.HasInstance )
//                PlayerProfile.Instance.Reset();
            
//            ScenesManagement.Instance.DeleteValues("PLAYER");
            
//            _saveLoaded = false;
//        }
//    }
//#if UNITY_EDITOR

//    //[MenuItem("Tools/Save/SetOnline")]
//    //public static void SetOnline()
//    //{
//    //    PlayerProfile.Instance.SetTutorialStep(100);
//    //    PlayerProfile.Instance.UnlockUpgrade(UpgradeType.Bunker);
//    //    PlayerProfile.Instance.UnlockUpgrade(UpgradeType.CanAttackPlayers);
        
//    //    if (PlayerProfile.Instance.GetLoverState() == LoverZState.State.Tutorial )
//    //        PlayerProfile.Instance.SetLoverState( LoverZState.State.Ok);

//    //    for (int i = 0; i < 3; i++)
//    //    {
//    //        PlayerProfile.Instance.AttackPlayer();
//    //    }
        
//    //    SaveGame.Save("PLAYER");


//    //    NetworkManager.Instance.CreatePlayer();
//    //}



//    [MenuItem("Tools/Save/View")]
//    public static void ViewFolder()
//    {
//        string savePath = GetSavePath();

//        Debug.Log(savePath);
//        if (Directory.Exists(savePath))
//        {
//            string path = GetSavePath();
//            path = path.Replace(@"/", @"\"); // explorer doesn't like front slashes
//            System.Diagnostics.Process.Start("explorer.exe", "/select," + path);
//        }
//    }


//#endif

//    public static void Save(params string[] containers)
//    {
//        if (Directory.Exists(GetSavePath()) == false)
//        {
//            Directory.CreateDirectory(GetSavePath());
//        }

//        foreach (var item in containers)
//        {
//            object data = ScenesManagement.Instance.GetData(item);
//            if (data == null)
//            {
//                Debug.LogError($"Can't find data {item}");
//                continue;
//            }

//            string str = JsonUtility.ToJson(data);
//            string path = GetSavePath($"{item}.dat");

//            using (FileStream fs = File.Create(path))
//            {
//                byte[] info = new UTF8Encoding(true).GetBytes(str);
//                fs.Write(info, 0, info.Length);
//            }

//            Debug.Log("Game data saved!" + path + "//////////" + str);
//            _saveLoaded = true;
//        }
//    }

//    public static string GetSlotName(int index)
//    {
//        string slot = _slots[index];
//        string path = Path.Combine(Path.Combine(Application.persistentDataPath, slot), DirectoryName);

//        if (Directory.Exists(path) == false)
//            return "None";

//        try
//        {
//            string dataFilePath = Path.Combine(path,  "Player.dat");
//            if (File.Exists(dataFilePath))
//            {
//                string content = File.ReadAllText(dataFilePath);
//                PlayerData save = JsonUtility.FromJson<PlayerData>(content);
//                return save.Name;
//            }
//        }
//        catch (Exception e)
//        {
//            // PlayerProfile is called during an awake
//            Debug.LogError(e.Message);
//        }
        
//        return "None";
//    }
    
//    public static void Load<T>(string fileName)
//    {
//        _saveLoaded = false;

//        _currentSlot = GetCurrentProfile();

//        string path = GetSavePath();

//        if (Directory.Exists(path) == false)
//            return;

//        Debug.Log($"LOAD {fileName}");

//        try
//        {
//            string dataFilePath = Path.Combine(path, fileName + ".dat");
//            if (File.Exists(dataFilePath))
//            {
//                string content = File.ReadAllText(dataFilePath);
//                T save = JsonUtility.FromJson<T>(content);

//                ScenesManagement.Instance.AddData(fileName.ToUpper(), save);
//                _saveLoaded = true;

//            }
//            else
//            {
//                _saveLoaded = false;
//            }
//        }
//        catch (Exception e)
//        {
//            // PlayerProfile is called during an awake
//            Debug.LogError(e.Message);
//        }

//    }

    
//    static string GetCurrentProfile()
//    {
//        string path = GetProfileDataPath();

//        if (File.Exists(path) == false )
//        {
//            return _slots[0];
//        }

//        try
//        {
//            string profile = File.ReadAllText(path);
//            return profile;
//        }
//        catch (Exception e)
//        {
//            return _slots[0];
//        }
//    }

//    static string GetSavePath()
//    {
//        return Path.Combine(Path.Combine(Application.persistentDataPath, _currentSlot), DirectoryName);
//    }

//    static string GetSavePath(string filename)
//    {
//        return Path.Combine(GetSavePath(), filename);
//    }

//    static string GetProfileDataPath()
//    {
//        return Path.Combine(Path.Combine(Application.persistentDataPath, DirectoryName), _slotFileName);
//    }

//    public static void SetSlot(int index)
//    {
//        _currentSlot = _slots[index];
        
//        if ( PlayerProfile.HasInstance )
//            PlayerProfile.Instance.Reset();
        
//        ScenesManagement.Instance.DeleteValues("PLAYER");

        
//        string path = GetProfileDataPath();
//        string directory = Path.GetDirectoryName(path);
        
//        if (Directory.Exists( directory) == false)
//        {
//            Directory.CreateDirectory(directory);
//        }
        
//        File.WriteAllText(path, _currentSlot);
//        Load<PlayerData>("Player");

        
        

//    }
//}

//[Serializable]
//public class ProfileData
//{
//    public string[] SlotNames;
//}