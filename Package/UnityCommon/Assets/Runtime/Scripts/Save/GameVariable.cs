﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public struct GameVariableNumber
{
    public string Name;
    public float Value;
}

[Serializable]
public struct GameVariableString
{
    public string Name;
    public string Value;
}

