using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class GlobalCanvas : MonoBehaviour
{
    public static GlobalCanvas Instance;


    Canvas _canvas;
    RectTransform _canvasRect;
    Vector2 _uiOffset;

    private void Awake()
    {
        Instance = this;
        
        _canvas = GetComponent<Canvas>();
        _canvasRect = _canvas.GetComponent<RectTransform>();
        _uiOffset = new Vector2((float)_canvasRect.sizeDelta.x / 2f, (float)_canvasRect.sizeDelta.y / 2f);
    }

    public void AddWorldUI( GameObject go , Vector3 worldPosition)
    {
        go.transform.SetParent(transform);

        // Get the position on the canvas
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(worldPosition);
        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * _canvasRect.sizeDelta.x, ViewportPosition.y * _canvasRect.sizeDelta.y);

        // Set the position and remove the screen offset
        go.transform.localPosition = proportionalPosition - _uiOffset;
    }
}
