﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;



namespace UnityCommon
{

    [CustomEditor(typeof(ResourcesManager))]
    public class ResourcesManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ResourcesManager manager = (ResourcesManager)target;

            EditorGUI.BeginChangeCheck();

            base.DrawDefaultInspector();

            GUILayout.Space(20);
            bool clickButton = GUILayout.Button("Generate ResourcesCollection.cs");

            var collections = manager.Collections.Select(x => x.Name);

            if ((EditorGUI.EndChangeCheck() == true ) || clickButton)
            {
                foreach (var resourcesCollection in manager.Collections)
                {
                    resourcesCollection.UniqueIdentifierID = new ResourcesCollectionID(resourcesCollection.name.GetHashCode());
                    EditorUtility.SetDirty(resourcesCollection);
                }
                
                GenerateEnumList(collections);

                AssetDatabase.Refresh();
            }
        }
        
        void GenerateEnumList(IEnumerable<string> names)
        {
            if (names == null )
            {
                Debug.LogError("There's no collection");
                return;
            }

            string directory = Directory.GetCurrentDirectory() + "/Assets/Scripts/Generated/";
            string filename = "ResourcesCollections.cs";
            string path = directory + filename;

            if (Directory.Exists(directory) == false)
                Directory.CreateDirectory(directory);

            if (File.Exists(path) == false)
            {
                using (File.Create(path))
                {
                }
            }

            string fileText = "public class ResourcesCollections \n{\n";

            Dictionary<int, string> collisionContainers = new Dictionary<int, string>();

            foreach (var item in names)
            {
                int hashCode = item.GetHashCode();

                if (collisionContainers.ContainsKey(hashCode))
                {
                    Debug.LogError("Can't create enum because there's a collision of enum value ; please change the name of \n" +
                                   collisionContainers[hashCode] + " or " + item); 

                    return;
                }

                collisionContainers.Add(hashCode, item);

                fileText += "public static readonly UnityCommon.ResourcesCollectionID " + item + " = (UnityCommon.ResourcesCollectionID)(" + hashCode + ");\n";
            }

            fileText += "}\n";

            File.WriteAllText(path, fileText);

            Debug.Log("[ResourcesCollection] Update enum file :" + filename);
        }

    }


}