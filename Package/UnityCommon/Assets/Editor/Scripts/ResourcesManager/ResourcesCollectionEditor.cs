﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;



namespace UnityCommon
{

    [CustomEditor(typeof(ResourcesCollection))]
    public class ResourceCollectionEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ResourcesCollection collection = (ResourcesCollection)target;
            string nameBefore = collection.Name;

            EditorGUI.BeginChangeCheck();

            base.DrawDefaultInspector();

            GUILayout.Space(20);
            bool clickButton = GUILayout.Button("Generate ResourcesCollection" + collection.name + ".cs");

            if ((EditorGUI.EndChangeCheck() == true && nameBefore == collection.Name ) || clickButton)
            {
                GenerateEnum(collection);

                AssetDatabase.Refresh();
            }
        }

        void GenerateEnum(ResourcesCollection collection)
        {
            if (string.IsNullOrEmpty(collection.Name))
            {
                Debug.LogError("ResourcesCollection name is not set !");
                return;
            }

            string directory = Directory.GetCurrentDirectory() + "/Assets/Scripts/Generated/";
            string filename = "ResourcesCollection" + collection.Name + ".cs";
            string path = directory + filename;

            if (Directory.Exists(directory) == false)
                Directory.CreateDirectory(directory);

            if (File.Exists(path) == false)
            {
                using (File.Create(path))
                {
                }
            }

            string fileText = "public enum ResourcesCollection" + collection.Name + " \n{\n";

            Dictionary<int, string> collisionContainers = new Dictionary<int, string>();

            int index = 0;
            foreach (var item in collection.AddToEnum)
            {
                fileText += item + " = " + index + ",\n";
                index++;
            }

            foreach (var item in collection.Objects)
            {
                int hashCode = item.name.GetHashCode();

                if (collisionContainers.ContainsKey(hashCode))
                {
                    Debug.LogError("Can't create enum because there's a collision of enum value ; please change the name of \n" +
                        collisionContainers[hashCode] + " or " + item.name); 

                    return;
                }

                collisionContainers.Add(hashCode, item.name);

                fileText += item.name + " = " + hashCode + ",\n";
            }

            fileText += "}\n";

            File.WriteAllText(path, fileText);

            Debug.Log("[ResourcesCollection] Update enum file :" + filename);
        }


    }


}