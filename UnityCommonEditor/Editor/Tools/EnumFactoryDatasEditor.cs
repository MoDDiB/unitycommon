﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace UnityCommon.Tools
{
    [CustomEditor(typeof(EnumFactoryDatas))]
    public class EnumFactoryDatasEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            EnumFactoryDatas enumFactory = target as EnumFactoryDatas;

            if (GUILayout.Button("Generate"))
            {
                Generate(enumFactory);
            }

        }

        #region Generate
        private void Generate(EnumFactoryDatas enumFactory)
        {
            string fileText = "public enum " + enumFactory.FactoryName + "Type" + Environment.NewLine + "{ " + Environment.NewLine;

            foreach (var item in enumFactory.EnumValues)
            {
                fileText += item + "," + Environment.NewLine;
            }

            fileText += "}" + Environment.NewLine;

            WriteFile(enumFactory.FactoryName, enumFactory.FactoryName + "Type", fileText);

            fileText = "using UnityEngine;" + Environment.NewLine +
            "public static class " + enumFactory.FactoryName + "Factory" + Environment.NewLine +
            "{" + Environment.NewLine +
            String.Format("    public static Base{0} Get{0}( {0}Type type )", enumFactory.FactoryName) + Environment.NewLine +
                       "    {" + Environment.NewLine +
                       "        switch (type)" + Environment.NewLine +
                       "        {" + Environment.NewLine;


            foreach (var item in enumFactory.EnumValues)
            {
                fileText += String.Format("            case {0}Type.{1}: return new {1}{0}();", enumFactory.FactoryName, item) + Environment.NewLine;
            }

            fileText +=
            "           default:" + Environment.NewLine +
                "           {" + Environment.NewLine +
                    "               Debug.LogError(\"AIFactory error : Can't instantiate \" + type);" + Environment.NewLine +
                    "               return null;" + Environment.NewLine +
                "           }" + Environment.NewLine +
                "       }" + Environment.NewLine +
                "   }" + Environment.NewLine;

            fileText += "}" + Environment.NewLine;

            fileText += String.Format("public partial class Base{0} ", enumFactory.FactoryName) + "{   }" + Environment.NewLine;

            string pathFiles = Directory.GetCurrentDirectory() + "/Assets/" + enumFactory.ClassPath;

            string fileName = Path.Combine(pathFiles, "Base" + enumFactory.FactoryName + ".cs");
            if (File.Exists(fileName) == false)
            {
                string content = String.Format("public partial class {0} ", "Base" + enumFactory.FactoryName) + "{}";
                File.WriteAllText(fileName, content);
            }

            foreach (var item in enumFactory.EnumValues)
            {
                fileText += String.Format("public partial class {1}{0}  : Base{0}", enumFactory.FactoryName, item) +
                    "{" + Environment.NewLine + Environment.NewLine + "}";

                fileName = Path.Combine(pathFiles, item + enumFactory.FactoryName + ".cs");
                Debug.Log("Create file " + fileName);

                if ( File.Exists(fileName) == false )
                {
                    string content = String.Format("public partial class {1}{0}  : Base{0}", enumFactory.FactoryName, item) + "{ " + Environment.NewLine + Environment.NewLine + "}";
                    File.WriteAllText(fileName, content);
                }
            }



            WriteFile(enumFactory.FactoryName, enumFactory.FactoryName + "Factory", fileText);


            AssetDatabase.Refresh();
        }

        String GetGeneratedPath(string directory)
        {
            return Directory.GetCurrentDirectory() + "/Assets/Scripts/Generated/" + directory + "/";
        }

        private void WriteFile(string folder, string fileName, string fileText)
        {
            string directory = GetGeneratedPath(folder);
            string filename = fileName + ".cs";
            string path = directory + filename;

            if (Directory.Exists(directory) == false)
                Directory.CreateDirectory(directory);

            if (File.Exists(path) == false)
            {
                using (File.Create(path))
                {
                }
            }

            File.WriteAllText(path, fileText);
        }

        #endregion Generate

    }
}
